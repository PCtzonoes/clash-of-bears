using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Soldier : LintBehaviour
{
	public Lint rotateSpeed = 150;
	public Lint moveSpeed = 10;

	public LintTransform target;

	private void Awake()
	{
		//disable on awake
		if (target == null)
			enabled = false;
	}

	private void FixedUpdate()
	{
		/*
		lintTransform.radians.y += rotateSpeed;

		LintVector3 direction = new LintVector3();
		direction.z = LintMath.Cos(lintTransform.radians.y);
		direction.x = LintMath.Sin(lintTransform.radians.y);

		lintTransform.position += direction * moveSpeed;
		*/
		

		//dirAtoB = B-A
		LintVector3 dirToTarget = target.position - lintTransform.position;
		Lint angle = LintMath.Atan2(dirToTarget.z, dirToTarget.x);
		lintTransform.radians.y = angle;
		dirToTarget = LintVector3.Normalized(dirToTarget);

		var movement = dirToTarget * moveSpeed;
		lintTransform.position +=movement ;
	}
}